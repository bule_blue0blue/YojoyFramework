﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Yojoy.Tech.Common.Core.Run;
using static Yojoy.Tech.Common.Core.Run.CommonGlobalUtility;


namespace Yojoy.Tech.U3d.Core.Editor
{
    public static class ModuleUtility
    {
        private static readonly DelayInitializationProperty<HashSet<string>>
            coreModuleIdsDelay = CreateDelayInitializationProperty(() =>
            {
                var tempModuleIds = new HashSet<string>();
                var asmdefPaths = DirectoryUtility
                    .GetPathsContainSonDirectory(YojoyEditorSettings
                            .CorePackageDirectory.Value,
                        path => path.EndsWith(".asmdef"));

                foreach (var asmdefPath in asmdefPaths)
                {
                    var moduleId = FileUtility
                        .GetFildIdWithoutExtension(asmdefPath);
                    tempModuleIds.Add(moduleId);
                }

                return tempModuleIds;
            });

        private static readonly DelayInitializationProperty<List<string>>
            moduleAssemblyPathsDelay = CreateDelayInitializationProperty(
                () =>
                {
                    var tempPaths = new List<string>();
                    var corePackagePaths = DirectoryUtility
                        .GetPathsContainSonDirectory(
                            YojoyEditorSettings.CorePackageDirectory.Value,
                            SelectAssemblyDef);
                    var extendPackagePaths = DirectoryUtility
                        .GetPathsContainSonDirectory(
                            YojoyEditorSettings.ExtendPackageDirectory.Value,
                            SelectAssemblyDef);
                    tempPaths.AddRange(corePackagePaths);
                    tempPaths.AddRange(extendPackagePaths);

                    return tempPaths;

                    bool SelectAssemblyDef(string path) =>
                        path.EndsWith(".asmdef");
                });


        private static readonly DelayInitializationProperty<
            Dictionary<string, string>> moduleFullDirectoriesDelay
            = CreateDelayInitializationProperty(() =>
            {
                var tempPathMap = new Dictionary<string, string>();
                var asmdefPaths = moduleAssemblyPathsDelay.Value;

                foreach (var asmdefPath in asmdefPaths)
                {
                    var directory = DirectoryUtility.GetFileLocDirectory(
                        asmdefPath);
                    var moduleId = FileUtility.GetFildIdWithoutExtension(
                        asmdefPath);
                    tempPathMap.Add(moduleId, directory);
                }

                return tempPathMap;
            });

        public static bool IsCoreModule(string moduleId) =>
            coreModuleIdsDelay.Value.Contains(moduleId);


        public static string GetModuleId(string path)
        {
            var fullDirectory = UnityDirectoryUtility.GetFullPath(path);
            var dictionary = moduleFullDirectoriesDelay.Value;
            var moduleId = dictionary.FindKeyValue(
                p => fullDirectory == p || fullDirectory.StartsWith(p)).Key;
            return moduleId;
        }
        
        
        
    }
}