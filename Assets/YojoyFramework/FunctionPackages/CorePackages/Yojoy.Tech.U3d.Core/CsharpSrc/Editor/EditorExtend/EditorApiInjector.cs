#region Comment Head

// Author:        liuruoyu1981
// CreateDate:    2020/1/11 15:24:54
// Email:         xxxx@qq.com

#endregion

using UnityEditor;
using Yojoy.Tech.U3d.Core.Run;

namespace Yojoy.Tech.U3d.Core.Editor
{
    public class EditorApiInjector : IEditorStateChangeHandler
    {
        public PlayModeStateChange ConcernedStateChange
            => PlayModeStateChange.EnteredEditMode;
        
        public void Handle()
        {
            YojoyEditorAgent.DisplayTip = UnityEditorUtility.DisplayTip;
            YojoyEditorAgent.GetBeautifiedJson = JsonNetUtility.GetBeautifiedJson;
        }
    }
}
