﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using Yojoy.Tech.Common.Core.Run;
using StringAssemblyMap = System.Collections.Generic.Dictionary<string,
    System.Reflection.Assembly>;
using static Yojoy.Tech.Common.Core.Run.CommonGlobalUtility;
using StateChangeHandlerMap = System.Collections.Generic.Dictionary<
    UnityEditor.PlayModeStateChange, System.Collections.Generic.List<
        Yojoy.Tech.U3d.Core.Editor.IEditorStateChangeHandler>>;

namespace Yojoy.Tech.U3d.Core.Editor
{
    [InitializeOnLoad]
    public static class UnityEditorEntrance
    {
        #region Entrance

        static UnityEditorEntrance()
        {
            InitMultiLanguageContext();
            EditorApplication.playModeStateChanged += OnPlayModeStateChanged;
            OnPlayModeStateChanged(PlayModeStateChange.EnteredEditMode);
            Debug.Log("YojoyFramework is started!");
        }

        #region MultiLanguage

        private const string MultiLanguageStringPrefsKey =
            "MultiLanguageStringPrefsKey";

        public static void UpdateLanguageType(LanguageType languageType)
        {
            EditorPrefs.SetString(MultiLanguageStringPrefsKey,
                languageType.ToString());
        }

        public static LanguageType GetCurrentLanguageType()
        {
            var languageType = EditorPrefs.GetString(MultiLanguageStringPrefsKey
                    , LanguageType.English.ToString())
                .AsEnum<LanguageType>();
            return languageType;
        }

        private static void InitMultiLanguageContext()
            => MultiLanguageString.SetLanguageType(GetCurrentLanguageType());

        #endregion

        #endregion

        #region Assemblies

        public static readonly DelayInitializationProperty<StringAssemblyMap>
            EditorAssemblies =
                CreateDelayInitializationProperty(InitEditorAssemblies);

        public static readonly DelayInitializationProperty<StringAssemblyMap>
            RunAssemblies = CreateDelayInitializationProperty(
                InitRunAssemblies);

        public static readonly DelayInitializationProperty<Assembly[]>
            EditorAssemblyArray = CreateDelayInitializationProperty(
                () => EditorAssemblies.Value.Values.ToArray());

        private static StringAssemblyMap InitRunAssemblies()
        {
            var tempStringAssemblyMap = new StringAssemblyMap();
            InitAssembliesAtDirectory(YojoyEditorSettings
                .CorePackageDirectory.Value, tempStringAssemblyMap, "Run");
            InitAssembliesAtDirectory(YojoyEditorSettings
                    .ExtendPackageDirectory.Value, tempStringAssemblyMap,
                "Run");
            return tempStringAssemblyMap;
        }

        private static StringAssemblyMap InitEditorAssemblies()
        {
            var tempStringAssemblyMap = new StringAssemblyMap();
            InitAssembliesAtDirectory(YojoyEditorSettings
                    .CorePackageDirectory.Value, tempStringAssemblyMap,
                "Editor");
            InitAssembliesAtDirectory(YojoyEditorSettings
                    .ExtendPackageDirectory.Value, tempStringAssemblyMap,
                "Editor");
            return tempStringAssemblyMap;
        }

        private static void InitAssembliesAtDirectory(string packageDirectory,
            StringAssemblyMap stringAssemblyMap, string assemblyTypeId)
        {
            var packageDirectories = DirectoryUtility.GetAllFirstSonDirectory(
                packageDirectory);

            foreach (var directory in packageDirectories)
            {
                var packageName = DirectoryUtility.GetDirectoryName(directory);
                var assemblyId = packageName + "." + assemblyTypeId;
                if (YojoyEditorSettings.IgnoreEditorAssemblyIds.Value
                    .Contains(assemblyId))
                {
                    continue;
                }

                var assemblyPath = UnityEditorConstant.ScriptAssembliesDirectory
                                       .Value + assemblyId + ".dll";
                if (!File.Exists(assemblyPath))
                {
                    continue;
                }

                var assembly = Assembly.LoadFile(assemblyPath);
                stringAssemblyMap.Add(assemblyId, assembly);
            }
        }

        #endregion

        #region Editor PlayModeStateChange Handle

        private static readonly
            DelayInitializationProperty<StateChangeHandlerMap>
            stateChangeHandlersDelay = CreateDelayInitializationProperty(() =>
            {
                var tempMap = new StateChangeHandlerMap();
                var handlers = ReflectionUtility.GetAllInstance<
                    IEditorStateChangeHandler>(EditorAssemblyArray.Value);

                foreach (var handler in handlers)
                {
                    if (!tempMap.ContainsKey(handler.ConcernedStateChange))
                    {
                        tempMap.Add(handler.ConcernedStateChange,
                            new List<IEditorStateChangeHandler>());
                    }

                    var targetHandlers = tempMap[handler.ConcernedStateChange];
                    if (!targetHandlers.Contains(handler))
                    {
                        targetHandlers.Add(handler);
                    }
                }

                return tempMap;
            });


        private static void OnPlayModeStateChanged(
            PlayModeStateChange playModeStateChange)
        {
            stateChangeHandlersDelay.Value.TryGetValue(playModeStateChange)
                .value?.ForEach(handler => handler.Handle());
        }

        #endregion
    }
}