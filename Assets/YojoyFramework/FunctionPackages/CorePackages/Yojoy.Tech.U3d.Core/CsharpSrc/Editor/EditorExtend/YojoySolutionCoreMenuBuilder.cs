#region Comment Head

// Author:        liuruoyu1981
// CreateDate:    2020/1/10 15:19:07
// Email:         xxxx@qq.com

#endregion

using System;
using System.Diagnostics;
using UnityEditor;
using UnityEngine;
using Yojoy.Tech.Common.Core.Run;
using Yojoy.Tech.U3d.Core.Run;

namespace Yojoy.Tech.U3d.Core.Editor
{
    public class YojoySolutionCoreMenuBuilder : IProjectExpanderMenuBuilder
    {
        private string directory;

        public Type ConcernedExpanderType => typeof(YojoySolutionExpander);

        public void AddMenuItem(GenericMenu genericMenu, string path)
        {
            directory = path;
            var openSandboxTip = MultiLanguageString.Create(
                "IO/Open sandbox directory",
                "IO/打开沙盒目录");
            genericMenu.AddItem(new GUIContent(openSandboxTip.Text),
                false, OpenSandbox, null);
            var cleanMetaTip = MultiLanguageString.Create(
                "IO/Clean meta file",
                "IO/清理meta文件");
            genericMenu.AddItem(new GUIContent(cleanMetaTip.Text),
                false, CleanMeta, path);
        }

        private void OpenSandbox(object data) =>
            Process.Start(Application.persistentDataPath);

        private void CleanMeta(object userData)
        {
            var metaPaths = DirectoryUtility.GetPathsContainSonDirectory(
                directory, p => p.EndsWith(".meta"));
            FileUtility.TryDeleteAll(metaPaths);
            UnityEditorUtility.DisplayTip("meta文件清理完毕！");
        }
    }
}