#region Comment Head

// Author:        liuruoyu1981
// CreateDate:    2020/1/7 16:38:50
// Email:         xxxx@qq.com

#endregion

using UnityEditor;

namespace Yojoy.Tech.U3d.Core.Editor
{
    public interface IEditorStateChangeHandler
    {
        PlayModeStateChange ConcernedStateChange { get; }

        void Handle();
    }
}
