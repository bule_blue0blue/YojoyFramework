#region Comment Head

// Author:        liuruoyu1981
// CreateDate:    2020/1/7 18:06:44
// Email:         xxxx@qq.com

#endregion

using System;
using UnityEditor;

namespace Yojoy.Tech.U3d.Core.Editor
{
    public interface IProjectExpanderMenuBuilder
    {
        Type ConcernedExpanderType { get; }

        void AddMenuItem(GenericMenu genericMenu, string path);
    }
}
