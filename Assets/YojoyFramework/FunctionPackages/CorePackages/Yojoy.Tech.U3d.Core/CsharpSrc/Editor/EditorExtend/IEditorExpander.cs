#region Comment Head

// Author:        liuruoyu1981
// CreateDate:    2020/1/7 17:33:49
// Email:         xxxx@qq.com

#endregion

using UnityEngine;

namespace Yojoy.Tech.U3d.Core.Editor
{
    public interface IEditorExpander
    {
        void Execute(Rect rect);
    }
}
