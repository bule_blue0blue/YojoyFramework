#region Comment Head

// Author:        liuruoyu1981
// CreateDate:    2020/1/7 17:48:50
// Email:         xxxx@qq.com

#endregion

using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Yojoy.Tech.Common.Core.Run;

namespace Yojoy.Tech.U3d.Core.Editor
{
    public abstract class AbstractProjectExpander : IProjectExpander
    {
        protected string Guid { get; private set; }

        protected string Path { get; private set; }

        public abstract void Execute(Rect rect);

        public void SaveContext(string guid, string path)
        {
            Guid = guid;
            Path = path;
        }

        public virtual bool CheckContext() => true;

        protected virtual float IconSize => EditorGUIUtility.singleLineHeight;

        protected Rect GetIconRect(float x, float y)
            => new Rect(x, y, IconSize, IconSize);

        private readonly List<IProjectExpanderMenuBuilder> menuBuilders;

        protected AbstractProjectExpander()
        {
            var allBuilders = ReflectionUtility.GetAllInstance<
                IProjectExpanderMenuBuilder>(UnityEditorEntrance
                .EditorAssemblyArray
                .Value);

            menuBuilders = allBuilders.FindAll(b => b.ConcernedExpanderType
                                                    == GetType());
        }

        protected virtual void MakeMenu()
        {
            var menu = new GenericMenu();

            foreach (var menuBuilder in menuBuilders)
            {
                menuBuilder.AddMenuItem(menu,Path);
            }
            
            menu.ShowAsContext();
        }
    }
}