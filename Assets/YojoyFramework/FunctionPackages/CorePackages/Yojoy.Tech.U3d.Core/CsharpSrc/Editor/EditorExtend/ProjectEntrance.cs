#region Comment Head

// Author:        liuruoyu1981
// CreateDate:    2020/1/9 16:43:55
// Email:         xxxx@qq.com

#endregion

using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Yojoy.Tech.Common.Core.Run;
using Yojoy.Tech.U3d.Core.Editor;
using static Yojoy.Tech.Common.Core.Run.CommonGlobalUtility;

namespace Yojoy.Tech.U3d.Core.Run
{
    public class ProjectEntrance : IEditorStateChangeHandler
    {
        public PlayModeStateChange ConcernedStateChange
            => PlayModeStateChange.EnteredEditMode;
        public void Handle()
        {
            EditorApplication.projectWindowItemOnGUI += ProjectWindowItemOnGui;
        }

        private readonly DelayInitializationProperty<List<IProjectExpander>>
            expandersDelay = CreateDelayInitializationProperty(() =>
            {
                var expanders = ReflectionUtility.GetAllInstance<
                    IProjectExpander>(UnityEditorEntrance.EditorAssemblyArray
                    .Value);
                return expanders;
            });
        
        private void ProjectWindowItemOnGui(string guid, Rect selectionrect)
        {
            var path = AssetDatabase.GUIDToAssetPath(guid);
            path = path.EnsureDirectoryEndFormat();

            foreach (var expander in expandersDelay.Value)
            {
                expander.SaveContext(guid,path);

                if (!expander.CheckContext())
                {
                    continue;
                }
                
                expander.Execute(selectionrect);
            }
        }
    }
}
