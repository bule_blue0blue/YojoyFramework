#region Comment Head

// Author:        liuruoyu1981
// CreateDate:    2020/1/7 17:34:35
// Email:         xxxx@qq.com

#endregion

namespace Yojoy.Tech.U3d.Core.Editor
{
    public interface IProjectExpander : IEditorExpander
    {
        void SaveContext(string guid, string path);

        bool CheckContext();
    }
}
