﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Yojoy.Tech.Common.Core.Run;
using static Yojoy.Tech.Common.Core.Run.CommonGlobalUtility;

namespace Yojoy.Tech.U3d.Core.Editor
{
    public static class YojoyEditorSettings
    {
        public static readonly string YojoyRootDirectoryId = "YojoyFramework";

        public static readonly DelayInitializationProperty<string> YojoyRootDirectory =
            CreateDelayInitializationProperty(() =>
                Application.dataPath + $"/{YojoyRootDirectoryId}/");

        public static readonly DelayInitializationProperty<string> PackageDirectory =
            CreateDelayInitializationProperty(() => $"{YojoyRootDirectory.Value}FunctionPackages/");

        public static readonly DelayInitializationProperty<string> CorePackageDirectory =
            CreateDelayInitializationProperty(() => $"{PackageDirectory.Value}CorePackages/");

        public static readonly DelayInitializationProperty<string> ExtendPackageDirectory =
            CreateDelayInitializationProperty(() => $"{PackageDirectory.Value}ExtendPackages/");

        public static readonly DelayInitializationProperty<HashSet<string>> IgnoreEditorAssemblyIds
            = CreateDelayInitializationProperty(() =>
            {
                var result = new HashSet<string>();
                result.Add("Yojoy.Tech.U3d.Third.Odin.Editor");
                result.Add("Yojoy.Tech.U3d.Third.Odin.Run");
                return result;
            });

        public static readonly DelayInitializationProperty<string> AssetsConst =
            CreateDelayInitializationProperty(() => "Assets/");

        public static readonly DelayInitializationProperty<string> AssetsHeadConst =
            CreateDelayInitializationProperty(() => $"Assets/{YojoyRootDirectoryId}/");


        public static string GetModuleAssetsDirectory(string moduleId,
            string moduleType)
        {
            var directory = CorePackageDirectory.Value
                            + $"{moduleId}/CsharpSrc/{moduleType}/Assets/";
            return directory;
        }
    }
}