#region Comment Head

// Author:        liuruoyu1981
// CreateDate:    2020/1/14 17:04:54
// Email:         xxxx@qq.com

#endregion

using System.IO;
using Newtonsoft.Json;

namespace Yojoy.Tech.U3d.Core.Editor
{
    public static class JsonNetUtility
    {
        public static string GetBeautifiedJson(string jsonContent)
        {
            var serializer = new JsonSerializer();
            var textReader = new StringReader(jsonContent);
            var jsonReader = new JsonTextReader(textReader);
            var obj = serializer.Deserialize(jsonReader);

            if (obj == null)
            {
                return jsonContent;
            }
            
            var textWriter = new StringWriter();
            var jsonWriter  = new JsonTextWriter(textWriter);
            jsonWriter.Formatting = Formatting.Indented;
            jsonWriter.Indentation = 4;
            jsonWriter.IndentChar = ' ';
            
            serializer.Serialize(jsonWriter,obj);
            return textWriter.ToString();
        }
    }
}
