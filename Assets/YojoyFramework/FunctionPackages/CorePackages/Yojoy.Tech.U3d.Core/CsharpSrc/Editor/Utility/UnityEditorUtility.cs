﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Yojoy.Tech.U3d.Core.Editor
{
    public static class UnityEditorUtility
    {
        public static void DisplayTip(string tipContent)
        {
            EditorUtility.DisplayDialog("Tip", tipContent, "OK");
        }
    }
}