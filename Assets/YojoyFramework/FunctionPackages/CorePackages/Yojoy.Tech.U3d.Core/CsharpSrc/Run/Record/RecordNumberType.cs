#region Comment Head

// Author:        liuruoyu1981
// CreateDate:    2020/1/14 14:21:02
// Email:         xxxx@qq.com

#endregion

namespace Yojoy.Tech.U3d.Core.Run
{
    [System.Serializable]
    public enum RecordNumberType
    {
        Unlimited,
        Singleton
    }
}