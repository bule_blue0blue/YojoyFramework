#region Comment Head

// Author:        liuruoyu1981
// CreateDate:    2020/1/14 14:21:02
// Email:         xxxx@qq.com

#endregion

using System;

namespace Yojoy.Tech.U3d.Core.Run
{
    [AttributeUsage(AttributeTargets.Class)]
    public class RecordAttribute : Attribute
    {
        public RecordNumberType RecordNumberType { get; }
        
        public RecordScopeType RecordScopeType { get; }

        public RecordAttribute(RecordNumberType numberType,
            RecordScopeType scopeType)
        {
            RecordNumberType = numberType;
            RecordScopeType = scopeType;
        }
    }
}
