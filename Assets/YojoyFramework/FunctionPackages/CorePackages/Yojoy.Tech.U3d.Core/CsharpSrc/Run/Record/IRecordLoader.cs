#region Comment Head

// Author:        liuruoyu1981
// CreateDate:    2020/1/14 14:21:02
// Email:         xxxx@qq.com

#endregion

using System;

namespace Yojoy.Tech.U3d.Core.Run
{
    public interface IRecordLoader
    {
        object LoadRecord(Type recordType, string recordName);

        void SaveRecord(IRecord record, bool deleteExist = false);
    }
}
