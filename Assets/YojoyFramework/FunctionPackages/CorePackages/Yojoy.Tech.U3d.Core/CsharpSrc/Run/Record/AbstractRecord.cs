#region Comment Head

// Author:        liuruoyu1981
// CreateDate:    2020/1/16 16:30:58
// Email:         xxxx@qq.com

#endregion

using Sirenix.OdinInspector;

namespace Yojoy.Tech.U3d.Core.Run
{
    public abstract class AbstractRecord : IRecord
    {
        public abstract string RecordName { get; protected set; }

        [Button("Save anytime","保存到最新",
            ButtonSizes.Medium)]
        protected virtual void SaveAnytime()
        {
            UnityRecordLoader.Instance.SaveRecord(this,true);
        }

        [Button("Try save","尝试保存",
            ButtonSizes.Medium)]
        protected virtual void TrySave()
        {
            UnityRecordLoader.Instance.SaveRecord(this);
        }
    }
}
