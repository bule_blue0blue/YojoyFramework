#region Comment Head

// Author:        liuruoyu1981
// CreateDate:    2020/1/11 15:23:00
// Email:         xxxx@qq.com

#endregion

using System;

namespace Yojoy.Tech.U3d.Core.Run
{
    public static class YojoyEditorAgent
    {
        public static Action<string> DisplayTip;
        public static Func<string, string> GetBeautifiedJson;
    }
}
