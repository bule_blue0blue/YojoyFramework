#region Comment Head

// Author:        liuruoyu1981
// CreateDate:    2020/1/20 17:23:46
// Email:         xxxx@qq.com

#endregion

using Yojoy.Tech.Common.Core.Run;

namespace Yojoy.Tech.U3d.Core.Run
{
    public class ChargingLine
    {
        [Inject]
        private USBSocket usbSocket;

        public void Connect() => usbSocket.Connect();
    }
}
