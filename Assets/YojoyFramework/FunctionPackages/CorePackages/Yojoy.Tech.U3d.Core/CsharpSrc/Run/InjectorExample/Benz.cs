#region Comment Head

// Author:        liuruoyu1981
// CreateDate:    2020/1/20 17:06:54
// Email:         xxxx@qq.com

#endregion

using UnityEngine;

namespace Yojoy.Tech.U3d.Core.Run
{
    public class Benz : ICar
    {
        public void Drive()
        {
            Debug.Log("This is benz!");
        }
    }
}
