#region Comment Head

// Author:        liuruoyu1981
// CreateDate:    2020/1/20 17:06:54
// Email:         xxxx@qq.com

#endregion

namespace Yojoy.Tech.U3d.Core.Run
{
    public interface ICar
    {
        void Drive();
    }
}
