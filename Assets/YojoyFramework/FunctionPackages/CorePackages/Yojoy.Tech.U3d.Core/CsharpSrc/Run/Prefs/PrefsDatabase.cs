#region Comment Head

// Author:        liuruoyu1981
// CreateDate:    2020/1/11 16:37:16
// Email:         xxxx@qq.com

#endregion

using System;
using Sirenix.OdinInspector;

namespace Yojoy.Tech.U3d.Core.Run
{
    [Serializable]
    public class PrefsDatabase
    {
        [FoldoutGroup("String prefs browser",
            "字符串数据")]
        [HideLabel]
        public readonly StringPrefsStorage StringPrefsStorage
        = new StringPrefsStorage();
        
        [FoldoutGroup("Int prefs browser",
            "Int数据")]
        [HideLabel]
        public readonly IntPrefsStorage IntPrefsStorage
        = new IntPrefsStorage();
        
        [FoldoutGroup("Float prefs browser",
            "Float数据")]
        [HideLabel]
        public readonly FloatPrefsStorage FloatPrefsStorage
        = new FloatPrefsStorage();
        
        [FoldoutGroup("Bool prefs browser",
            "布尔数据")]
        [HideLabel]
        public readonly BoolPrefsStorage BoolPrefsStorage
        = new BoolPrefsStorage();
    }
}
