#region Comment Head

// Author:        liuruoyu1981
// CreateDate:    2020/1/11 11:29:44
// Email:         xxxx@qq.com

#endregion

namespace Yojoy.Tech.U3d.Core.Run
{
    [System.Serializable]
    public class StringPrefsValueSet : AbstractPrefsValueSetGeneric<
        StringPrefsValue, string>
    {
    }
}