#region Comment Head

// Author:        liuruoyu1981
// CreateDate:    2020/1/10 17:21:32
// Email:         xxxx@qq.com

#endregion

using UnityEngine;

namespace Yojoy.Tech.U3d.Core.Run
{
    [System.Serializable]
    public abstract class AbstractPrefsValueGeneric<TValue>
        : IPrefsValueGeneric<TValue>
    {
        [SerializeField]
        private string key;

        public string Key => key;

        [SerializeField]
        private TValue value;
        public TValue Value => value;

        [SerializeField]
        [TextArea(2,3)]
        private string description;
        public string Description => description;
        
        
        public void Init(string key, TValue value, string description)
        {
            this.key = key;
            this.value = value;
            this.description = description;
        }
    }
}
