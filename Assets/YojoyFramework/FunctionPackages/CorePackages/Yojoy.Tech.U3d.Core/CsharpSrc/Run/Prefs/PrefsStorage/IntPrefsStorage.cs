#region Comment Head

// Author:        liuruoyu1981
// CreateDate:    2020/1/11 12:36:41
// Email:         xxxx@qq.com

#endregion

using System;

namespace Yojoy.Tech.U3d.Core.Run
{
    [Serializable]
    public class IntPrefsStorage : AbstractPrefsStorageGeneric<
    IntPrefsValueSet,IntPrefsValue,int>
    {
        
    }
}
