#region Comment Head

// Author:        liuruoyu1981
// CreateDate:    2020/1/11 12:36:41
// Email:         xxxx@qq.com

#endregion

using Sirenix.OdinInspector;
using UnityEngine;
using Yojoy.Tech.Common.Core.Run;

namespace Yojoy.Tech.U3d.Core.Run
{
    [System.Serializable]
    public abstract class AbstractPrefsStorageGeneric<TPrefsValueSet,
        TPrefsValue, TValue>
        where TPrefsValue : IPrefsValueGeneric<TValue>, new()
        where TPrefsValueSet : AbstractPrefsValueSetGeneric<TPrefsValue, TValue>
        , new()
    {
        #region Prefs Query

        [SerializeField]
        [BoxGroup("Query", "查询")]
        [LabelText("Target prefs id", "目标数据ID")]
        private string targetPrefsId;


        [BoxGroup("Query", "查询")]
        [Button("Query", "查询")]
        private void Query()
        {
            if (!targetPrefsId.IsValid())
            {
                YojoyEditorAgent.DisplayTip("The prefs id is null!");
                return;
            }

            var prefsValue = GetPrefsValue(targetPrefsId);
            if (prefsValue == null)
            {
                YojoyEditorAgent.DisplayTip("Cannot find target prefs!");
                return;
            }

            targetPrefsValue = prefsValue;
        }


        [ShowIf("TargetPrefsValueIsNotNull")]
        [SerializeField]
        [LabelText("Target prefs value", "目标数据")]
        private TPrefsValue targetPrefsValue;


        private bool TargetPrefsValueIsNotNull() => targetPrefsValue != null;

        #endregion

        #region Prefs Value Set

        [HideLabel] [SerializeField]
        private TPrefsValueSet prefsValueSet = new TPrefsValueSet();

        public TPrefsValueSet PrefsValueSet => prefsValueSet;

        #endregion


        #region Prefs Get And Save

        public TPrefsValue GetPrefsValue(string key)
            => prefsValueSet.GetPrefsValue(key);

        public void SavePrefsValue(TPrefsValue prefsValue)
            => PrefsValueSet.AddOrUpdatePrefsValue(prefsValue);

        #endregion
    }
}