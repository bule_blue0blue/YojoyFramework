#region Comment Head

// Author:        liuruoyu1981
// CreateDate:    2020/1/11 17:25:24
// Email:         xxxx@qq.com

#endregion

using System;
using System.IO;
using Sirenix.OdinInspector;
using UnityEngine;
using Yojoy.Tech.Common.Core.Run;

namespace Yojoy.Tech.U3d.Core.Run
{
    [Serializable]
    public class PrefsSpanner
    {
        [HideLabel] [SerializeField] private PrefsDatabase prefsDatabase;

        private string DatabasePath => UnityGlobalUtility.IsEditorMode
            ? Application.dataPath + "/.prefs.binary"
            : Application.persistentDataPath + "/.prefs.binary";

        private void LoadPrefsDatabase()
        {
            if (!File.Exists(DatabasePath))
            {
                prefsDatabase = new PrefsDatabase();
                return;
            }

            var bytes = File.ReadAllBytes(DatabasePath);
            if (bytes.Length == 0)
            {
                return;
            }

            prefsDatabase = SerializaUtility.DeSerialize<
                PrefsDatabase>(bytes);
        }

        #region Singleton

        private PrefsSpanner() => LoadPrefsDatabase();

        public static readonly PrefsSpanner Instance = new PrefsSpanner();

        #endregion

        #region Save Delete

        [Button("Save", "保存",
            ButtonSizes.Medium)]
        private void Save()
        {
            var bytes = SerializaUtility.Serialize(prefsDatabase);
            FileUtility.WriteAllBytes(DatabasePath, bytes);
        }

        private void DeleteAll()
        {
            prefsDatabase = null;
            Save();
        }

        #endregion

        #region SetPrefs

        private void SetValue<TPrefsStorage, TPrefsValue, TValue>
        (
            string key,
            TValue value,
            AbstractPrefsStorageGeneric<TPrefsStorage, TPrefsValue, TValue>
                prefsValues,
            string description
        )
            where TPrefsValue : IPrefsValueGeneric<TValue>, new()
            where TPrefsStorage : AbstractPrefsValueSetGeneric<TPrefsValue,
                TValue>, new()
        {
            var prefs = new TPrefsValue();
            prefs.Init(key, value, description);
            prefsValues.PrefsValueSet.AddOrUpdatePrefsValue(prefs);
            Save();
        }

        public void SetString(string key, string value,
            string description = null)
        {
            SetValue(key, value, prefsDatabase.StringPrefsStorage, description);
        }

        public void SetBool(string key, bool value,
            string description = null)
        {
            SetValue(key, value, prefsDatabase.BoolPrefsStorage, description);
        }

        public void SetFloat(string key, float value,
            string description = null)
        {
            SetValue(key, value, prefsDatabase.FloatPrefsStorage, description);
        }

        public void SetInt(string key, int value,
            string description = null)
        {
            SetValue(key, value, prefsDatabase.IntPrefsStorage, description);
        }

        #endregion

        #region GetPrefs

        private TValue GetValue<TPrefsStorage, TPrefsValue, TValue>
        (
            string key,
            TValue defaultValue,
            AbstractPrefsStorageGeneric<TPrefsStorage, TPrefsValue, TValue>
                prefsStorage, string description
        )
            where TPrefsValue : IPrefsValueGeneric<TValue>, new()
            where TPrefsStorage :
            AbstractPrefsValueSetGeneric<TPrefsValue, TValue>,
            new()
        {
            var prefs = prefsStorage.GetPrefsValue(key);
            if (prefs != null)
            {
                return prefs.Value;
            }

            SetValue(key, defaultValue, prefsStorage, description);
            return defaultValue;
        }

        public string GetString(string key, string defaultValue,
            string description = null)
        {
            var prefs = GetValue(key, description, prefsDatabase
                .StringPrefsStorage, description);
            return prefs;
        }

        public bool GetBool(string key, bool defaultValue,
            string description = null)
        {
            var prefs = GetValue(key, defaultValue, prefsDatabase
                .BoolPrefsStorage, description);
            return prefs;
        }

        public float GetFloat(string key, float defaultValue,
            string description = null)
        {
            var prefs = GetValue(key, defaultValue, prefsDatabase
                .FloatPrefsStorage, description);
            return prefs;
        }

        public int GetInt(string key, int defaultValue,
            string description = null)
        {
            var prefs = GetValue(key, defaultValue,
                prefsDatabase.IntPrefsStorage,
                description);
            return prefs;
        }

        #endregion
    }
}