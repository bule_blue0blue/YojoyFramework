#region Comment Head

// Author:        liuruoyu1981
// CreateDate:    2020/1/11 11:29:44
// Email:         xxxx@qq.com

#endregion

using System.Collections.Generic;

namespace Yojoy.Tech.U3d.Core.Run
{
    [System.Serializable]
    public abstract class AbstractPrefsValueSetGeneric<TPrefsValue, TValue>
        where TPrefsValue : IPrefsValueGeneric<TValue>, new()
    {
        
        public readonly List<TPrefsValue> PrefsValues
        = new List<TPrefsValue>();

        public TPrefsValue GetPrefsValue(string key)
        {
            var prefsValue = PrefsValues.Find(p => p.Key == key);
            return prefsValue;
        }

        public void AddOrUpdatePrefsValue(TPrefsValue prefsValue)
        {
            var existPrefs = GetPrefsValue(prefsValue.Key);
            if (existPrefs != null)
            {
                PrefsValues.Remove(existPrefs);
            }
            
            PrefsValues.Add(prefsValue);
        }

        public void DeletePrefsValue(string key)
        {
            var existPrefs = GetPrefsValue(key);
            if (existPrefs != null)
            {
                PrefsValues.Remove(existPrefs);
            }
        }
    }
}