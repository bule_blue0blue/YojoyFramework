#region Comment Head

// Author:        liuruoyu1981
// CreateDate:    2020/1/10 17:19:25
// Email:         xxxx@qq.com

#endregion

namespace Yojoy.Tech.U3d.Core.Run
{
    public interface IPrefsValueGeneric<TValue>
    {
        string Key { get; }
        
        TValue Value { get; }
        
        string Description { get; }

        void Init(string key, TValue value, string description);
    }
}
