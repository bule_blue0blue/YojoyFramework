﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Yojoy.Tech.Common.Core.Run
{
    [Serializable]
    public enum LanguageType 
    {
        English,
        Chinese
    }
}