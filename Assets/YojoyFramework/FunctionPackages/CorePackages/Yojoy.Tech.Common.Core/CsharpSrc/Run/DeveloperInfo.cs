﻿
namespace Yojoy.Tech.Common.Core.Run
{
    [System.Serializable]
    public class DeveloperInfo
    {
        public string Name;
        public string Email;
    }
}