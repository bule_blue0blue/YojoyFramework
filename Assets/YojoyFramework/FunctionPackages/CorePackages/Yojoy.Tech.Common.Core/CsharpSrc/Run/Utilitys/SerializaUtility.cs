#region Comment Head

// Author:        liuruoyu1981
// CreateDate:    2020/1/11 17:38:57
// Email:         xxxx@qq.com

#endregion

using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Yojoy.Tech.Common.Core.Run
{
    public static class SerializaUtility
    {
        public static byte[] Serialize(object instance)
        {
            if (instance == null)
            {
                return null;
            }

            using (var ms = new MemoryStream())
            {
                var bf = new BinaryFormatter();
                bf.Serialize(ms,instance);
                var bytes = new byte[ms.Length];
                Buffer.BlockCopy(ms.GetBuffer(),0,bytes,
                    0,(int)ms.Length);
                return bytes;
            }
        }

        public static T DeSerialize<T>(byte[] value) where T : class, new()
        {
            if (value == null)
            {
                return default;
            }

            using (var ms = new MemoryStream(value))
            {
                var bf = new BinaryFormatter();
                var instance = (T) bf.Deserialize(ms);
                return instance;
            }
        }
    }
}
