﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Yojoy.Tech.Common.Core.Run
{
    public static class FileUtility
    {
        public static string GetFildIdWithoutExtension(string path)
        {
            var id = Path.GetFileNameWithoutExtension(path);
            if (id == null)
            {
                throw new Exception(
                    $"An error occurred while getting the target file name!");
            }

            return id;
        }

        public static void WriteAllText(string path, string content)
        {
            DirectoryUtility.EnsureDirectoryExist(path);
            File.WriteAllText(path, content);
        }


        public static void TryDelete(string path)
        {
            if (File.Exists(path))
            {
                File.Delete(path);
            }
        }

        public static void TryDeleteAll(List<string> paths)
        {
            foreach (var path in paths)
            {
                TryDelete(path);
            }
        }

        public static void WriteAllBytes(string path, byte[] bytes)
        {
            DirectoryUtility.EnsureDirectoryExist(path);
            TryDelete(path);
            File.WriteAllBytes(path,bytes);
        }
    }
}