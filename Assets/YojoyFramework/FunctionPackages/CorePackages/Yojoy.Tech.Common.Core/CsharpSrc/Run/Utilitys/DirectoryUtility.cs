﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace Yojoy.Tech.Common.Core.Run
{
    public static class DirectoryUtility
    {
        public static List<string> GetAllFirstSonDirectory(string directory,
            Func<string, bool> selector = null)
        {
            if (string.IsNullOrEmpty(directory))
            {
                return null;
            }

            var directories = new List<string>();
            var sonDirectories = Directory.GetDirectories(directory).ToList();

            foreach (var sonDirectory in sonDirectories)
            {
                var tempDirectory = sonDirectory.EnsureDirectoryEndFormat();
                tempDirectory = tempDirectory.Replace("\\", "/");
                directories.Add(tempDirectory);
            }

            if (selector != null)
            {
                directories = directories.Where(selector).ToList();
            }

            return directories;
        }

        public static string GetDirectoryName(string directory)
        {
            if (directory.EndsWith("/"))
            {
                directory = directory.Substring(0, directory.Length - 1);
            }

            var array = directory.Split('/');
            var directoryName = array.Last();
            return directoryName;
        }

        public static void TryCreateDirectory(string targetDirectory, bool isHide = false)
        {
            if (Directory.Exists(targetDirectory))
            {
                return;
            }

            Directory.CreateDirectory(targetDirectory);
            if (isHide)
            {
                File.SetAttributes(targetDirectory, FileAttributes.Hidden);
            }
        }

        public static void EnsureDirectoryExist(string targetDirectory)
        {
            var lastIndex = targetDirectory.LastIndexOf("/", StringComparison.Ordinal);
            var lastDirectory = targetDirectory.Substring(0, lastIndex);
            TryCreateDirectory(lastDirectory);
        }

        private static void GetAllDirectories(string directory, List<string> directories)
        {
            if (string.IsNullOrEmpty(directory) || !Directory.Exists(directory))
            {
                return;
            }

            if (Directory.GetDirectories(directory).Length == 0)
            {
                return;
            }

            var sonDirectories = Directory.GetDirectories(directory);

            foreach (var sonDirectory in sonDirectories)
            {
                var tempDirectory = sonDirectory.EnsureDirectoryEndFormat();
                tempDirectory = tempDirectory.Replace("\\", "/");
                directories.Add(tempDirectory);
                GetAllDirectories(tempDirectory, directories);
            }
        }


        public static List<string> GetAllDirectoryContainSelf(string directory,
            Func<string, bool> selector = null)
        {
            var directories = new List<string>();
            directories.Add(directory);

            GetAllDirectories(directory, directories);
            if (selector != null)
            {
                directories = directories.Where(selector).ToList();
            }

            return directories;
        }

        public static List<string> GetPathsContainSonDirectory(string directory,
            Func<string, bool> selector = null, Func<string, bool> directoryFilter = null)
        {
            EnsureDirectoryExist(directory);
            var directories = GetAllDirectoryContainSelf(directory, directoryFilter);
            var paths = new List<string>();

            foreach (var dir in directories)
            {
                var files = Directory.GetFiles(dir).ToList();
                files = files.Select(p => p.Replace("\\", "/")).ToList();
                paths.AddRange(files);
            }

            if (selector != null)
            {
                paths = paths.Where(selector).ToList();
            }

            return paths;
        }

        public static string GetFileLocDirectory(string path)
        {
            if (!File.Exists(path))
            {
                throw new Exception("There are no files on the destination path!");
            }

            var lastIndex = path.LastIndexOf('/');
            var directory = path.Substring(0, lastIndex);
            directory = directory.EnsureDirectoryEndFormat();
            return directory;
        }
    }
}