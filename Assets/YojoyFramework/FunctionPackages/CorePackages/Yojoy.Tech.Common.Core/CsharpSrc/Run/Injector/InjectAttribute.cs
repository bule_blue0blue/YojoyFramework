#region Comment Head

// Author:        liuruoyu1981
// CreateDate:    2020/1/19 16:09:27
// Email:         xxxx@qq.com

#endregion

using System;

namespace Yojoy.Tech.Common.Core.Run
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class InjectAttribute : Attribute
    {
        
    }
}
