#region Comment Head

// Author:        liuruoyu1981
// CreateDate:    2020/1/19 16:14:36
// Email:         xxxx@qq.com

#endregion

namespace Yojoy.Tech.Common.Core.Run
{
    public interface IInjector
    {
        TTargetType Get<TTargetType>(bool useReflection)
            where TTargetType : class;
    }
}
