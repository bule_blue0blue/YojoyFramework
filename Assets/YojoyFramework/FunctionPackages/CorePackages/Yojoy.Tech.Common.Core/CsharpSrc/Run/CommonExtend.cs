﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Yojoy.Tech.Common.Core.Run
{
    public static class CommonExtend
    {
        #region Common

        public static TValue As<TValue>(this object instance)
            where TValue : class
        {
            var result = instance as TValue;
            return result;
        }

        #endregion

        #region String

        public static string EnsureDirectoryEndFormat(this string directory)
        {
            if (!directory.EndsWith("/"))
            {
                directory += "/";
            }

            return directory;
        }

        public static string EveryToBig(this string str)
        {
            var sb = new StringBuilder();

            foreach (var c in str)
            {
                if (!char.IsLetter(c))
                {
                    sb.Append(c);
                }
                else
                {
                    var bigC = char.ToUpper(c);
                    sb.Append(bigC);
                }
            }

            var bigStr = sb.ToString();
            return bigStr;
        }

        public static bool IsValid(this string str)
        {
            var result = !string.IsNullOrEmpty(str) &&
                         !string.IsNullOrWhiteSpace(str);
            return result;
        }

        #endregion

        #region Enum

        public static TEnum AsEnum<TEnum>(this string str) where TEnum : Enum
        {
            var result = (TEnum) Enum.Parse(typeof(TEnum), str);
            return result;
        }

        public static List<TEnum> GetAllEnumValues<TEnum>() where TEnum : Enum
        {
            var enumList = new List<TEnum>();
            var enums = Enum.GetValues(typeof(TEnum));

            foreach (var @enum in enums)
            {
                var value = @enum.ToString();
                var enumValue = AsEnum<TEnum>(value);
                enumList.Add(enumValue);
            }

            return enumList;
        }

        #endregion

        #region Reflection

        public static List<TATTR> GetAttributes<TATTR>(this Type type)
            where TATTR : Attribute
        {
            var attributes = type.GetCustomAttributes(typeof(TATTR),
                true).Select(t => (TATTR) t).ToList();
            return attributes;
        }

        public static TATTR GetSingleAttribute<TATTR>(this Type type)
            where TATTR : Attribute
        {
            var attributes = type.GetAttributes<TATTR>();
            if (attributes.Count > 1)
            {
                throw new Exception($"The number of target" +
                                    $"{typeof(TATTR).Name}"
                                    + "is greater than 1!");
            }

            if (attributes.Count == 0)
            {
                return null;
            }

            var singleAttr = attributes[0];
            return singleAttr;
        }

        #endregion

        #region Dictionary

        public static KeyValuePair<TKey, TValue> FindKeyValue<TKey, TValue>
        (
            this IDictionary<TKey, TValue> dictionary,
            Func<TValue, bool> valueSelector
        )
        {
            foreach (var kv in dictionary)
            {
                if (valueSelector(kv.Value))
                {
                    return kv;
                }
            }

            return default;
        }

        public static (bool hasValue, TValue value) TryGetValue<TKey, TValue>
            (this IDictionary<TKey, TValue> dictionary, TKey key)
        {
            if (!dictionary.ContainsKey(key))
            {
                return (false, default);
            }

            var value = dictionary[key];
            return (true, value);
        }

        #endregion
    }
}