﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Yojoy.Tech.Common.Core.Editor;
using Yojoy.Tech.Common.Core.Run;

namespace Yojoy.Tech.Common.Core.Editor
{
    public class CsharpScriptAppender : AbstractScriptAppender
    {
        public void AppendSingleComment(string comment)
            => AppendLine($"// {comment}");

        public void AppendMultiComment(params string[] comments)
        {
            AppendLine("/*");

            foreach (var comment in comments)
            {
                AppendLine("* " + comment);
            }

            AppendLine("*/");
            AppendLine();
        }

        public void AppendUsingNameSpace(params string[] nameSpaces)
        {
            foreach (var nameSpace in nameSpaces)
            {
                AppendLine($"using {nameSpace};");
            }

            AppendLine();
        }

        public void AppendComment
        (
            string bodyComment,
            List<string> paramNames = null,
            List<string> paramComments = null
        )
        {
            AppendLine("/// <summary>");
            AppendLine("/// " + bodyComment);
            AppendLine("/// </summary>");

            if (paramNames == null)
            {
                return;
            }

            if (paramComments == null)
            {
                foreach (var paramName in paramNames)
                {
                    AppendLine("/// <param name=\"" +
                               paramName + "\"></param>");
                }
            }
            else
            {
                for (int index = 0; index < paramNames.Count; index++)
                {
                    var name = paramNames[index];
                    var comment = paramComments[index];
                    AppendFormatLine("/// <param name=\"{0}\">{1}</param>",
                        name, comment);
                }
            }
        }

        public void AppendCommentHeader(string coderName, string coderEmail)
        {
            AppendLine("#region Comment Head");
            AppendLine();
            AppendLine($"// Author:        {coderName}");
            AppendLine(
                $"// CreateDate:    {CommonGlobalUtility.NowDateString()}");
            AppendLine($"// Email:         {coderEmail}");
            AppendLine();
            AppendLine("#endregion");
            AppendLine();
        }

        public void AppendFooter()
        {
            ToLeft();
            AppendLine("}");
            ToLeft();
            AppendLine("}");
            AppendLine();
        }
    }
}