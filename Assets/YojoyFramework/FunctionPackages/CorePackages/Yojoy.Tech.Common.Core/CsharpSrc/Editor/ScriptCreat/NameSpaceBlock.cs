﻿using System;

namespace Yojoy.Tech.Common.Core.Editor
{
    public class NameSpaceBlock : IDisposable
    {
        private readonly CsharpScriptAppender appender;

        public NameSpaceBlock(CsharpScriptAppender appender,
            string @namespace)
        {
            this.appender = appender;
            appender.AppendLine($"namespace {@namespace}");
            appender.AppendLeftBracketsAndToRight();
        }

        public void Dispose() => appender.AppendToLeftAndRightBrackets();
    }
}