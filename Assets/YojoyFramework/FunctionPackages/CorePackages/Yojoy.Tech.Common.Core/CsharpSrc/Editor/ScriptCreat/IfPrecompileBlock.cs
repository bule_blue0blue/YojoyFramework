﻿using System;
using System.Collections.Generic;

namespace Yojoy.Tech.Common.Core.Editor
{
    public class IfPrecompileBlock : IDisposable
    {
        private readonly CsharpScriptAppender appender;
        private readonly List<string> instructions;

        public IfPrecompileBlock(CsharpScriptAppender appender
            , List<string> instructions)
        {
            this.appender = appender;
            this.instructions = instructions;

            AppendIfStart();
        }

        private void AppendIfStart()
        {
            for (int index = 0; index < instructions.Count; index++)
            {
                var instruct = instructions[index];

                appender.Append(index == 0
                    ? $"#if {instruct}"
                    : $" && {instruct}");
            }

            appender.AppendLine();
        }

        public void Dispose()
        {
            if (instructions == null || instructions.Count == 0)
            {
                return;
            }

            appender.AppendLine();
            appender.AppendLine("#endif");
        }
    }
}