#region Comment Head

// Author:        liuruoyu1981
// CreateDate:    2020/1/14 13:17:30
// Email:         xxxx@qq.com

#endregion

using Yojoy.Tech.Common.Core.Run;

namespace Yojoy.Tech.U3d.Odin.Editor
{
    public abstract class AbstractOnceFirstActiveItem : IOnActive
    {
        private bool isActivated;

        public void OnActive()
        {
            if (isActivated)
            {
                return;
            }

            DoActive();
            isActivated = true;
        }

        protected abstract void DoActive();
    }
}