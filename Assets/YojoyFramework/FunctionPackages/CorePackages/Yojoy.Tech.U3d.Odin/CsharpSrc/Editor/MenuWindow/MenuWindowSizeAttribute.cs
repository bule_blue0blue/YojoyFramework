﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Yojoy.Tech.U3d.Odin.Editor
{
    [AttributeUsage(AttributeTargets.Class)]
    public class MenuWindowSizeAttribute : Attribute
    {
        public int Min { get; private set; }
        
        public int Max { get; private set; }

        public MenuWindowSizeAttribute(int min, int max)
        {
            Min = min;
            Max = max;
        }
    }
}