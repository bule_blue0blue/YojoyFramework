﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities.Editor;
using UnityEditor;
using UnityEngine;
using Yojoy.Tech.Common.Core.Run;
using Yojoy.Tech.U3d.Core.Editor;
using Yojoy.Tech.U3d.Core.Run;

namespace Yojoy.Tech.U3d.Odin.Editor
{
    [MenuWindowSize(500, 500)]
    [MenuWindowTitle("Function Center", "功能中心")]
    public class FunctionCenterWindow
        : AbstractMenuWindowGeneric<FunctionCenterWindow>
    {
        #region Open Window

        [MenuItem("YojoyFramework/Function Center %k")]
        private static void Open() => OpenSingleWindow();

        public static void OpenTargetMenu(Type menuType)
        {
            OpenSingleWindow();
            SingleWindow.Value.SwitchToTargetMenu(menuType);
        }

        #endregion

        #region BuildTree

        protected override void BuildFixedMenus(OdinMenuTree odinMenuTree)
        {
            base.BuildFixedMenus(odinMenuTree);

            BuildMenuObject<CsharpScaffold>("Csharp Scaffold",
                "Csharp脚手架");
            var prefsSpannerTip = MultiLanguageString
                .Create("Prefs spanner", "Prefs工具");
            AddItemAndCacheIndex(OdinMenuTree, prefsSpannerTip.Text,
                PrefsSpanner.Instance);
            BuildMenuObject<SwissArmyKnife>("Swiss army knife",
                "瑞士军刀");
            BuildMenuObject<PrecompileModifier>("Precompile Modifier",
                "预编译修改");
        }

        private void BuildMenuObject<TMenuObject>(string englishTitle,
            string chineseTitle)
            where TMenuObject : class, new()
        {
            var finalEnglishTitle = englishTitle ?? typeof(TMenuObject).Name;
            AddItemAndCacheIndex(OdinMenuTree, MultiLanguageString.Create(
                    finalEnglishTitle, chineseTitle).Text,
                ReflectionUtility.CreateInstance<TMenuObject>());
        }

        #endregion

        #region TopToolbar

        protected override void BuildTopToolbar()
        {
            OdinMenuTree.DefaultMenuStyle.IconSize = 28.00f;
            OdinMenuTree.Config.DrawSearchToolbar = true;
        }

        protected override void OnBeginDrawEditors()
        {
            if (OdinMenuTree == null)
            {
                return;
            }

            var selected = MenuTree.Selection.FirstOrDefault();
            var toolbarHeight = MenuTree.Config.SearchToolbarHeight;

            SirenixEditorGUI.BeginHorizontalToolbar(toolbarHeight);
            {
                if (selected != null)
                {
                    GUILayout.Label(selected.Name);
                }

                DrawLanguageSwitch();
            }

            SirenixEditorGUI.EndHorizontalToolbar();

            void DrawLanguageSwitch()
            {
                var languageType = UnityEditorEntrance.GetCurrentLanguageType();
                var switchLanguageContent = $"Language : {languageType}";
                if (SirenixEditorGUI.ToolbarButton(
                    new GUIContent(switchLanguageContent)))
                {
                    MakeSwitchLanguageMenu();
                }
            }

            void MakeSwitchLanguageMenu()
            {
                var genericMenu = new GenericMenu();

                var languageTypes =
                    CommonExtend.GetAllEnumValues<LanguageType>();

                foreach (var languageType in languageTypes)
                {
                    genericMenu.AddItem(new GUIContent(
                            languageType.ToString()), false,
                        SwitchLanguage, languageType.ToString());
                }

                genericMenu.ShowAsContext();
            }

            void SwitchLanguage(object data)
            {
                var languageType = ((string) data).AsEnum<LanguageType>();
                MultiLanguageString.SetLanguageType(languageType);
                UnityEditorEntrance.UpdateLanguageType(languageType);
                ForceMenuTreeRebuild();
                OpenLastMenu();

                var switchTip = MultiLanguageString.Create(
                    $"Yojoy已切换为{languageType}!",
                    $"Yojoy has been switched to {languageType}!");
                Debug.Log(switchTip.Text);
            }
        }

        #endregion
    }
}