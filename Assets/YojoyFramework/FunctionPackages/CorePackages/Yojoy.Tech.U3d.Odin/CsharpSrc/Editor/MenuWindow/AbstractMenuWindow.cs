﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector.Editor;
using UnityEngine;
using Yojoy.Tech.Common.Core.Run;
using Yojoy.Tech.U3d.Core.Editor;
using static Yojoy.Tech.Common.Core.Run.CommonGlobalUtility;

namespace Yojoy.Tech.U3d.Odin.Editor
{
    public abstract class AbstractMenuWindow : OdinMenuEditorWindow
    {
        #region Switch Menu

        private Type currentMenuType;
        protected int CurrentMenuIndex;
        private readonly List<Type> menuTypes = new List<Type>();

        protected OdinMenuTree OdinMenuTree { get; private set; }

        protected void SwitchToTargetMenu(Type menuType)
        {
            var index = menuTypes.FindIndex(m => m == menuType);
            SwitchToTargetMenu(index);
        }

        protected virtual void SwitchToTargetMenu(int index)
        {
            if (index < 0 || index >= menuTypes.Count)
            {
                return;
            }

            var item = OdinMenuTree.MenuItems[index];
            item.Value.As<IOnActive>()?.OnActive();
            item.Select();
        }

        #endregion

        #region Build Menu

        private readonly DelayInitializationProperty<List<IWindowMenuBuilder>>
            menuBuilders = CreateDelayInitializationProperty(
                () =>
                {
                    var result = ReflectionUtility
                        .GetAllInstance<IWindowMenuBuilder>(
                            UnityEditorEntrance.EditorAssemblyArray
                                .Value);
                    return result;
                });

        protected virtual void BuildTopToolbar()
        {
        }

        protected virtual void BuildFixedMenus(OdinMenuTree odinMenuTree)
        {
        }

        protected virtual void BuildDynamicMenus(OdinMenuTree odinMenuTree)
        {
            var windowType = GetType();
            var builders = menuBuilders.Value
                .FindAll(m => m.MapWindowType == windowType);
            builders.ForEach(m => m.BuildMenu(odinMenuTree));
        }

        protected void OpenLastMenu() => SwitchToTargetMenu(CurrentMenuIndex);

        protected virtual void AddItemAndCacheIndex(OdinMenuTree odinMenuTree,
            string menuTitle, object menuObject)
        {
            odinMenuTree.Add(menuTitle, menuObject);
            odinMenuTree.MenuItems.Last().OnLeftClick += OnItemSelect;

            var menuType = menuObject.GetType();
            if (!menuTypes.Contains(menuType))
            {
                menuTypes.Add(menuType);
            }

            void OnItemSelect(OdinMenuItem targetItem)
            {
                CurrentMenuIndex = OdinMenuTree.MenuItems
                    .FindIndex(item => item == targetItem);
                targetItem.As<IOnActive>()?.OnActive();
            }
        }

        protected override OdinMenuTree BuildMenuTree()
        {
            OdinMenuTree = new OdinMenuTree();
            BuildTopToolbar();
            BuildFixedMenus(OdinMenuTree);
            BuildDynamicMenus(OdinMenuTree);
            OpenLastMenu();
            return OdinMenuTree;
        }

        #endregion
    }
}