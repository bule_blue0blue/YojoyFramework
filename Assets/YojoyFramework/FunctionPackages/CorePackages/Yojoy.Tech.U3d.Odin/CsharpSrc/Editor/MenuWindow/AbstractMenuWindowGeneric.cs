﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Yojoy.Tech.Common.Core.Run;
using Yojoy.Tech.U3d.Odin.Editor;
using TypeToWindowMap = System.Collections.Generic.Dictionary<
    System.Type, object>;
using static Yojoy.Tech.Common.Core.Run.CommonGlobalUtility;

public abstract class AbstractMenuWindowGeneric<TWindow>
    : AbstractMenuWindow
    where TWindow : AbstractMenuWindow
{
    #region Single Window

    private static Type WindowType => typeof(TWindow);

    private static readonly DelayInitializationProperty<TypeToWindowMap>
        singleWindows = CreateDelayInitializationProperty(
            () => new TypeToWindowMap());

    protected static readonly DelayInitializationProperty<TWindow>
        SingleWindow = CreateDelayInitializationProperty(
            () =>
            {
                var window = GetWindow<TWindow>();
                singleWindows.Value.Add(WindowType, window);
                return window;
            });

    #endregion


    #region Pipeline

    protected override void OnDestroy()
    {
        base.OnDestroy();

        SingleWindow.SetNull();
        singleWindows.Value.Remove(GetType());
    }

    #endregion


    #region Open

    protected static void OpenSingleWindow()
    {
        InitTitle();
        InitSize();
        FocusAndShow();

        void InitTitle()
        {
            var titleAttribute =
                WindowType.GetSingleAttribute<MenuWindowTitleAttribute>();
            var titleString = titleAttribute == null
                ? MultiLanguageString.Create(
                    WindowType.Name,
                    WindowType.Name)
                : titleAttribute.TitleString;
            SingleWindow.Value.titleContent = new GUIContent(titleString.Text);
        }

        void InitSize()
        {
            var sizeAttribute =
                WindowType.GetSingleAttribute<MenuWindowSizeAttribute>();
            var min = sizeAttribute?.Min ?? 300;
            var max = sizeAttribute?.Max ?? 600;
            var initSize = new Vector2(min, max);
            SingleWindow.Value.minSize = initSize;
        }

        void FocusAndShow()
        {
            SingleWindow.Value.Focus();
            SingleWindow.Value.Show();
        }
    }

    #endregion
}