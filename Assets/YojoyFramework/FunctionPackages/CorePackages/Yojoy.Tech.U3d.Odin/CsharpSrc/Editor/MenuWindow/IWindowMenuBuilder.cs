﻿using System;
using Sirenix.OdinInspector.Editor;

namespace Yojoy.Tech.U3d.Odin.Editor
{
    public interface IWindowMenuBuilder
    {
        Type MapWindowType { get; }

        void BuildMenu(OdinMenuTree menuTree);
    }
}