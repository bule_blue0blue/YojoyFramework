#region Comment Head

// Author:        liuruoyu1981
// CreateDate:    2020/1/16 16:41:21
// Email:         xxxx@qq.com

#endregion

using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;
using Yojoy.Tech.Common.Core.Run;
using Yojoy.Tech.U3d.Core.Editor;
using Yojoy.Tech.U3d.Core.Run;
using static Yojoy.Tech.Common.Core.Run.CommonGlobalUtility;

namespace Yojoy.Tech.U3d.Odin.Editor
{
    [System.Serializable]
    public class RecordSpanner
    {
        [SerializeField] [LabelText("Record name", "记录名")]
        private string recordName;

        private readonly DelayInitializationProperty<List<Type>>
            recordTypesDelay = CreateDelayInitializationProperty(() =>
            {
                var allTypes = new List<Type>();
                var editorTypes = ReflectionUtility.GetTypeList<IRecord>(
                    false, false,
                    UnityEditorEntrance.EditorAssemblyArray.Value);
                var runTypes = ReflectionUtility.GetTypeList<IRecord>(
                    false, false,
                    UnityEditorEntrance.EditorAssemblyArray.Value);
                allTypes.AddRange(editorTypes);
                allTypes.AddRange(runTypes);

                return allTypes;
            });

        private void MakeCreateRecordMenu(RecordNumberType recordNumberType)
        {
            var genericMenu = new GenericMenu();

            foreach (var recordType in recordTypesDelay.Value)
            {
                var recordAttribute = recordType
                    .GetSingleAttribute<RecordAttribute>();
                if (!SelectRecordType(recordAttribute))
                {
                    continue;
                }

                genericMenu.AddItem(new GUIContent(recordType.Name),
                    false, CreateRecord, recordType);
            }

            genericMenu.ShowAsContext();

            bool SelectRecordType(RecordAttribute recordAttribute)
            {
                var result = recordAttribute.RecordNumberType
                             == recordNumberType;
                return result;
            }

            void CreateRecord(object data)
            {
                var type = (Type) data;
                UnityRecordLoader.Instance
                    .LoadRecord(type, recordName);
                AssetDatabase.Refresh();
            }
        }


        [Button("Create single record","创建单例记录",
            ButtonSizes.Medium)]
        private void CreateSingleRecord()
        {
            MakeCreateRecordMenu(RecordNumberType.Singleton);
        }

        [Button("Create unlimited record","" +
                                          "创建非单例记录",ButtonSizes.Medium)]
        private void CreateUnlimitedRecord()
        {
            if (!recordName.IsValid())
            {
                UnityEditorUtility.DisplayTip("Record name cannot be null!");
                return;
            }
            
            MakeCreateRecordMenu(RecordNumberType.Unlimited);
        }
    }
}