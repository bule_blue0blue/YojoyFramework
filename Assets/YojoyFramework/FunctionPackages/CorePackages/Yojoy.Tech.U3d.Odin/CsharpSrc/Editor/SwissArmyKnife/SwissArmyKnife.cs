#region Comment Head

// Author:        liuruoyu1981
// CreateDate:    2020/1/16 16:41:21
// Email:         xxxx@qq.com

#endregion

using Sirenix.OdinInspector;
using UnityEngine;

namespace Yojoy.Tech.U3d.Odin.Editor
{
    [System.Serializable]
    public class SwissArmyKnife
    {
        [SerializeField]
        [HideLabel]
        [BoxGroup("Record spanner","记录工具")]
        private RecordSpanner recordSpanner;

        public RecordSpanner RecordSpanner => recordSpanner;
    }
}
