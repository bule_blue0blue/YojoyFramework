﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;
using Yojoy.Tech.Common.Core.Editor;
using Yojoy.Tech.Common.Core.Run;
using Yojoy.Tech.U3d.Core.Editor;
using Yojoy.Tech.U3d.Core.Run;


namespace Yojoy.Tech.U3d.Odin.Editor
{
    [System.Serializable]
    public class CsharpScaffold : IOnActive
    {
        #region Visualization

        public static string OutputDirectoryPrefsKey
            => UnityGlobalUtility.GetPrefsKey("OutputDirectory",
                typeof(CsharpScaffold));


        [BoxGroup("Base Config", "基础配置")]
        [TextArea(2, 3)]
        [SerializeField]
        [ReadOnly]
        [OnValueChanged("OnOutputDirectoryChanged")]
        private string outputDirectory;

        private void OnOutputDirectoryChanged()
        {
            EditorPrefs.SetString(OutputDirectoryPrefsKey, outputDirectory);
        }

        [LabelText("CopyRight Content", "版权信息")]
        [TextArea(3, 6)]
        [SerializeField]
        [OnValueChanged("OnCopyRightContentChanged")]
        private string copyRightContent;

        private string CopyRightPrefsKey => UnityGlobalUtility.GetPrefsKey(
            "CopyRightContent", typeof(CsharpScaffold));

        private void OnCopyRightContentChanged()
        {
            EditorPrefs.SetString(CopyRightPrefsKey, copyRightContent);
        }

        [BoxGroup("Global Config", "全局配置")]
        [OnValueChanged("OnGlobalNameSpaceChanged")]
        [LabelText("Global Namespace", "全局命名空间")]
        [SerializeField]
        private string globalNameSpace;

        private string GlobalNamespacePrefsKey =>
            UnityGlobalUtility.GetPrefsKey(
                "GlobalNamespace", typeof(CsharpScaffold));

        private void OnGlobalNameSpaceChanged()
        {
            EditorPrefs.SetString(GlobalNamespacePrefsKey, globalNameSpace);
            IfPrecompile = globalNameSpace.EveryToBig().Replace(
                ".", "_");
            OnIfPrecompileChanged();
            TryCloseAutoAddIfPrecompile();
        }

        private void TryCloseAutoAddIfPrecompile()
        {
            requireAddIfPrecompile =
                !ModuleUtility.IsCoreModule(globalNameSpace);
        }

        [BoxGroup("Global Config", "全局配置")]
        [LabelText("If Precompile", "IF预编译指令")]
        [OnValueChanged("OnIfPrecompileChanged")]
        [SerializeField]
        private string IfPrecompile;

        private string IfPrecompilePrefsKey => UnityGlobalUtility
            .GetPrefsKey("IfPrecompile", typeof(CsharpScaffold));

        [BoxGroup("Global Config", "全局配置")]
        [LabelText("Require Add IfPrecompile",
            "是否需要添加if预编译指令")]
        [SerializeField]
        private bool requireAddIfPrecompile = true;

        private void OnIfPrecompileChanged()
        {
            EditorPrefs.SetString(IfPrecompilePrefsKey, IfPrecompile);
        }

        [BoxGroup("Developer Info", "开发者信息")]
        [HideLabel][SerializeField]
        private DeveloperInfo developerInfo;

        [LabelText("Csharp Create Infos", "脚本创建信息")]
        [HideLabel]
        [SerializeField]
        private List<CsharpCreateInfo> csharpCreateInfos;

        #endregion

        #region GetScriptContent

        private string GetScriptContent(CsharpScriptAppender appender,
            CsharpCreateInfo createInfo)
        {
            #region Local Method

            TryAppendCopyRight();

            if (requireAddIfPrecompile)
            {
                using (new IfPrecompileBlock(appender,
                    new List<string> {IfPrecompile}))
                {
                    appender.AppendLine();
                    appender.AppendCommentHeader(developerInfo.Name,
                        developerInfo.Email);

                    using (new IfPrecompileBlock(appender,
                        createInfo.IfPrecompileInsturctions))
                    {
                        using (new NameSpaceBlock(appender, globalNameSpace))
                        {
                            AppendScriptContent();
                        }
                    }
                }
            }
            else
            {
                appender.AppendCommentHeader(developerInfo.Name,
                    developerInfo.Email);
                using (new NameSpaceBlock(appender, globalNameSpace))
                {
                    AppendScriptContent();
                }
            }

            return appender.ToString();

            void AppendScriptContent()
            {
                var classHeadString = string.Format("public {0} {1}",
                    GetScriptKeyword(createInfo.ScriptType),
                    createInfo.ScriptName);
                appender.AppendLine(classHeadString);
                appender.AppendLeftBracketsAndToRight();
                appender.AppendLine();
                appender.AppendToLeftAndRightBrackets();
            }

            string GetScriptKeyword(CsharpScriptType csharpScriptType)
            {
                switch (csharpScriptType)
                {
                    case CsharpScriptType.Class:
                        return "class";
                    case CsharpScriptType.AbstractClass:
                        return "abstract class";
                    case CsharpScriptType.Interface:
                        return "interface";
                    case CsharpScriptType.Enum:
                        return "enum";
                    case CsharpScriptType.Struct:
                        return "struct";
                    default:
                        throw new ArgumentOutOfRangeException(
                            nameof(csharpScriptType), csharpScriptType, null);
                }
            }

            void TryAppendCopyRight()
            {
                if (!copyRightContent.IsValid())
                {
                    return;
                }

                appender.AppendLine(copyRightContent);
                appender.AppendLine();
            }

            #endregion
        }

        #endregion

        #region Context Handle

        public CsharpScaffold()
        {
            copyRightContent = EditorPrefs.GetString(CopyRightPrefsKey);
            globalNameSpace = EditorPrefs.GetString(GlobalNamespacePrefsKey);
            IfPrecompile = EditorPrefs.GetString(IfPrecompilePrefsKey);
            LoadDeveloperInfo();
            UpdateOutputDirectory();
        }

        private readonly string DeveloperPrefsKey = UnityGlobalUtility
            .GetPrefsKey("DeveloperInfo", typeof(CsharpScaffold));

        private void LoadDeveloperInfo()
        {
            var jsonString = EditorPrefs.GetString(DeveloperPrefsKey);
            if (string.IsNullOrEmpty(jsonString))
            {
                return;
            }

            developerInfo = JsonUtility.FromJson<DeveloperInfo>(jsonString);
        }

        private void SaveDeveloperInfo()
        {
            var jsonString = JsonUtility.ToJson(developerInfo);
            EditorPrefs.SetString(DeveloperPrefsKey, jsonString);
        }


        private void UpdateOutputDirectory()
        {
            outputDirectory = EditorPrefs.GetString(OutputDirectoryPrefsKey);
            var moduleId = ModuleUtility.GetModuleId(outputDirectory);
            if (moduleId == null)
            {
                return;
            }

            globalNameSpace = moduleId;
            OnGlobalNameSpaceChanged();
        }

        #endregion

        #region Create Script

        private string GetScriptOutPath(CsharpCreateInfo createInfo
            , string directory)
        {
            var finalPath = directory.EnsureDirectoryEndFormat()
                            + createInfo.ScriptName + ".cs";
            return finalPath;
        }

        private bool CheckCreateInfoError()
        {
            if (!outputDirectory.IsValid())
            {
                UnityEditorUtility.DisplayTip(
                    "No script creation directory selected!");
                return true;
            }

            if (!Directory.Exists(outputDirectory))
            {
                UnityEditorUtility.DisplayTip(
                    "The target directory is not a valid directory!");
                return true;
            }

            if (csharpCreateInfos == null || csharpCreateInfos.Count == 0)
            {
                UnityEditorUtility.DisplayTip(
                    "Create information cannot be empty!");
                return true;
            }

            foreach (var createInfo in csharpCreateInfos)
            {
                if (!createInfo.ScriptName.IsValid())
                {
                    UnityEditorUtility.DisplayTip(
                        "Script name cannot be empty!");
                    return true;
                }


                if (!globalNameSpace.IsValid())
                {
                    UnityEditorUtility.DisplayTip(
                        "Global namespace cannot be empty!");
                    return true;
                }

                var finalPath = GetScriptOutPath(createInfo, outputDirectory);
                if (File.Exists(finalPath))
                {
                    UnityEditorUtility.DisplayTip(
                        "Script already exists on the target path!");
                    return true;
                }
            }

            return false;
        }

        private void CreateScript()
        {
            if (CheckCreateInfoError())
            {
                return;
            }

            var appender = new CsharpScriptAppender();

            foreach (var createInfo in csharpCreateInfos)
            {
                appender.Clean();
                var scriptPath = GetScriptOutPath(createInfo, outputDirectory);
                var scriptContent = GetScriptContent(appender, createInfo);
                FileUtility.WriteAllText(scriptPath, scriptContent);
            }

            SaveDeveloperInfo();
            AssetDatabase.Refresh();
            UnityEditorUtility.DisplayTip("Script is created!");
        }

        #endregion

        #region Visual Invoke

        [Button("Create script in select directory",
            "选择目录创建脚本", ButtonSizes.Medium)]
        private void CreateScriptInSelectDirectory()
        {
            var defaultDirectory = outputDirectory ?? Application.dataPath;
            var targetDirectory = EditorUtility.OpenFolderPanel(
                "Please select script output directory!",
                defaultDirectory, null);

            if (!Directory.Exists(targetDirectory))
            {
                UnityEditorUtility.DisplayTip("No directory selected!");
                return;
            }

            outputDirectory = targetDirectory.EnsureDirectoryEndFormat();
            CreateScript();
        }

        [Button("Create script in output directory",
            "在输出目录下创建脚本", ButtonSizes.Medium)]
        private void CreateScriptInOutputDirectory() => CreateScript();

        #endregion

        public void OnActive() => UpdateOutputDirectory();
    }
}